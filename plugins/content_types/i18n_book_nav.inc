<?php

/**
 * @file
 * Define the Ctools plugin for integration with Panels.
 *
 * This plugin is based on the Book plugin that ships with Ctools. All code is
 * almost a verbatim copy of the Book plugin.
 *
 * Original patch by joel_osc, updated and enhanced by wadmiraal.
 */

if (module_exists('i18n_book_navigation')) {
  $plugin = array(
    'single' => TRUE,
    'title' => t('i18n Book navigation'),
    'icon' => 'icon_node.png',
    'description' => t('The translated book menu belonging to the current book node.'),
    'required context' => new ctools_context_required(t('Node'), 'node'),
    'category' => t('Node'),
  );
}

/**
 * Implements hook_PLUGIN_content_type_render().
 */
function i18n_book_navigation_i18n_book_nav_content_type_render($subtype, $conf, $panel_args, $context) {
  $node = isset($context->data) ? clone($context->data) : NULL;
  $block = new stdClass();
  $block->module = 'i18n_book_nav';
  $block->title = t('i18n Book navigation');

  if ($node) {
    // The node view hook contains the logic for rendering the book navigation.
    i18n_book_navigation_node_view($node, 'full');

    // If nothing was added to the render array, we fall back to the default
    // Book module logic.
    if (!isset($node->content['book_navigation'])) {
      book_node_view($node, 'full');
    }

    $block->content = isset($node->content['book_navigation']) ? $node->content['book_navigation'] : '';
    $block->delta = $node->nid;
  }
  else {
    $block->content = t('Book navigation goes here.');
    $block->delta = 'unknown';
  }

  return $block;
}

/**
 * Implements hook_PLUGIN_content_type_admin_title().
 */
function i18n_book_navigation_i18n_book_nav_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" i18n book navigation', array('@s' => $context->identifier));
}

/**
 * Implements hook_PLUGIN_content_type_edit_form().
 */
function i18n_book_navigation_i18n_book_nav_content_type_edit_form($form, &$form_state) {
  // Provide a blank form so we have a place to have context setting.
  return $form;
}
