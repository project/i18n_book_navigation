<?php

/**
 * @file
 * Unit tests for the Book translation module.
 */

class I18nBookNavigationTestBase extends DrupalWebTestCase {

  /**
   * {@inheritDoc}
   */
  public function setUp() {
    parent::setUp('i18n_book_navigation', 'translation');

    // Create and login user.
    $this->admin_user = $this->drupalCreateUser(array_keys(module_invoke_all('permission')));

    $this->drupalLogin($this->admin_user);

    $this->enableLanguages(array('fr'));
    $this->configureI18nDetection();
    $this->makeBooksTranslatable();
    $this->enableI18nBookNavigationBlock();
  }

  /**
   * {@inheritDoc}
   *
   * Add the ability to assert a text is found a certain number of times.
   */
  public function assertText($text, $message = '', $num = 1, $group = 'Other') {
    if ($num === 1) {
      return parent::assertText($text, $message, $group);
    }
    else {
      $matches = array();

      $page_content = filter_xss(preg_replace('/<title>(.+)<\/title>/', '', $this->drupalGetContent()), array());

      return $this->assert($num === preg_match_all("/($text)/", $page_content, $matches), $message . " (found $num times)", $group);
    }
  }

  /**
   * Enable languages.
   *
   * @param array $languages
   *   Each item should be a language code.
   */
  protected function enableLanguages($languages) {
    foreach ($languages as $language) {
      $edit = array(
        'langcode' => $language,
      );

      $this->drupalPost('admin/config/regional/language/add', $edit, t("Add language"));
    }
  }

  /**
   * Enables translations on book nodes.
   *
   * @param bool $use_entity_translation
   *   (optional) Wether to use Entity translation. Defaults to false.
   */
  protected function makeBooksTranslatable($use_entity_translation = FALSE) {
    $edit = array(
      'language_content_type' => $use_entity_translation ? 4 : 2,
    );

    $this->drupalPost('admin/structure/types/manage/book', $edit, t("Save content type"));

    // Make fields translatable.
    if ($use_entity_translation) {
      $this->drupalGet('admin/structure/types/manage/book/fields');

      // Title field.
      $this->clickLink('replace');
      $edit = array(
        'enabled' => 1,
      );
      $this->drupalPost(NULL, $edit, t("Save settings"));

      // Body field.
      $this->drupalGet('admin/structure/types/manage/page/fields/body');
      $edit = array(
        'field[translatable]' => 1,
      );
      $this->drupalPost(NULL, $edit, t("Save settings"));
    }
  }

  /**
   * Enable the i18n_book_navigation block.
   */
  protected function enableI18nBookNavigationBlock() {
    $edit = array(
      "blocks[i18n_book_navigation_i18n_book_navigation][region]" => 'sidebar_first',
    );

    $this->drupalPost('admin/structure/block', $edit, t("Save blocks"));
  }

  /**
   * Disable the i18n_book_navigation block.
   */
  protected function disableI18nBookNavigationBlock() {
    $edit = array(
      "blocks[i18n_book_navigation_i18n_book_navigation][region]" => '-1',
    );

    $this->drupalPost('admin/structure/block', $edit, t("Save blocks"));
  }

  /**
   * Creates a book outline in the desired language.
   *
   * @param string $language
   *   (optional) The language of the nodes. Defaults to 'en'.
   * @param bool $use_entity_translation
   *   (optional) Wether to use Entity translation. Defaults to false.
   *
   * @return array
   *   The book bid as the 1st key and the nodes in an array as the 2nd key..
   */
  protected function createBookOutline($language = 'en', $use_entity_translation = FALSE) {
    $this->book = $this->createBookNode('new', NULL, $language, $use_entity_translation);

    // Add page hierarchy to book.
    // Book - Node 0
    //  |- Node 1
    //    |- Node 2
    //    |- Node 3
    //  |- Node 4
    //  |- Node 5
    $nodes = array();

    // Node 1.
    $nodes[] = $this->book;
    $nodes[] = $this->createBookNode($this->book->nid, NULL, $language, $use_entity_translation);

    // Node 2.
    $nodes[] = $this->createBookNode($this->book->nid, $nodes[1]->book['mlid'], $language, $use_entity_translation);

    // Node 3.
    $nodes[] = $this->createBookNode($this->book->nid, $nodes[1]->book['mlid'], $language, $use_entity_translation);

    // Node 4.
    $nodes[] = $this->createBookNode($this->book->nid, NULL, $language, $use_entity_translation);

    // Node 5.
    $nodes[] = $this->createBookNode($this->book->nid, NULL, $language, $use_entity_translation);

    return array($this->book->nid, $nodes);
  }

  /**
   * Translate nodes.
   *
   * @param array $nodes
   *   The list of nodes to translate.
   * @param string $language
   *   The language to which to translate the nodes.
   * @param bool $use_entity_translation
   *   (optional) Wether to use Entity translation. Defaults to false.
   *
   * @return array
   *   A list of translation nodes.
   */
  protected function translateNodes($nodes, $language, $use_entity_translation = FALSE) {
    $new_nodes = array();

    foreach ($nodes as $node) {
      $new_nodes[] = $this->translateBookNode($node, $language, $use_entity_translation);
    }

    return $new_nodes;
  }

  /**
   * Create a book node.
   *
   * Port of the book module test cases.
   * @see BooktestCase::createBookNode()
   */
  protected function createBookNode($bid, $plid = NULL, $language = NULL, $use_entity_translation = FALSE) {
    static $number = 0;

    $edit = array();

    // Weird bug in ET? Don't know why when the Book ID is given, the key is
    // correct, and when not, the key is "und".
    $language_key = ($bid == 'new' && $use_entity_translation) || !$use_entity_translation ? LANGUAGE_NONE : $language;

    $title_key = $use_entity_translation ? "title_field[$language_key][0][value]" : "title";
    $edit[$title_key] = $number . ' - Simpletest test node ' . $this->randomName(10);

    $edit["body[$language_key][0][value]"] = 'Simpletest test body ' . $this->randomName(32) . ' ' . $this->randomName(32);

    $edit['book[bid]'] = $bid;

    $edit['language'] = isset($language) ? $language : LANGUAGE_NONE;

    if (isset($plid)) {
      $this->drupalPost('node/add/book', $edit, t('Change book (update list of parents)'));

      $edit['book[plid]'] = $plid;

      $this->drupalPost(NULL, $edit, t('Save'));
    }
    else {
      $this->drupalPost('node/add/book', $edit, t('Save'));
    }

    $number++;

    return $this->drupalGetNodeByTitle($edit[$title_key]);
  }

  /**
   * Translate book node.
   *
   * @param object $node
   *   The book node to translate.
   * @param string $language
   *   The language to translate the node in.
   * @param bool $use_entity_translation
   *   (optional) Wether to use Entity translation. Defaults to false.
   *
   * @return object
   *   The new translated node
   */
  protected function translateBookNode($node, $language, $use_entity_translation = FALSE) {
    $edit = array();

    $body_language = $use_entity_translation ? $language : LANGUAGE_NONE;

    $title_key = $use_entity_translation ? "title_field[$language][0][value]" : "title";

    $edit[$title_key] = $language . ' - Simpletest test node ' . $this->randomName(10);

    $edit["body[$body_language][0][value]"] = 'Simpletest test body ' . $this->randomName(32) . ' ' . $this->randomName(32);

    if ($use_entity_translation) {
      $this->drupalPost('node/' . $node->nid . '/edit/add/' . $node->language . '/' . $language, $edit, t("Save"));
    }
    else {
      $edit['language'] = $language;

      $this->drupalPost('node/add/book', $edit, t("Save"));
    }

    if (!$use_entity_translation) {
      // Add the translation link, because Drupal stores the translation link in
      // the form cache, not in the hidden form elements.
      $new_node = $this->drupalGetNodeByTitle($edit[$title_key]);

      $new_node->tnid = $node->nid;

      node_save($new_node);
    }
    else {
      // Clone trick using multiple casting.
      $new_node = (object) (array) $node;

      $new_node->title = $edit[$title_key];
    }

    return $new_node;
  }

  /**
   * Configure the locale selection.
   */
  protected function configureI18nDetection($use_entity_translation = FALSE) {
    $edit = array(
      "language[enabled][locale-session]" => 0,
      "language[enabled][locale-url]" => 1,
    );

    if ($use_entity_translation) {
      $edit["language_content[enabled][locale-session]"] = 1;
      $edit["language_content[enabled][locale-url]"] = 1;
    }

    $this->drupalPost('admin/config/regional/language/configure', $edit, t("Save settings"));

    // Reset caches.
    drupal_static_reset('locale_url_outbound_alter');
    drupal_static_reset('language_list');
  }

  /**
   * Test outline translations.
   *
   * @param bool $use_entity_translation
   *   (optional) Wether to use Entity Translation or not. Defaults to false.
   * @param string $group
   *   (optional) Which group to use for the assertions. Defaults to
   *   'testOutlineTranslation'.
   */
  protected function outlineTranslation($use_entity_translation = FALSE, $group = 'testOutlineTranslation') {
    // Create 2 outlines.
    list($bid, $nodes) = $this->createBookOutline('en', $use_entity_translation);
    $fr_nodes = $this->translateNodes($nodes, 'fr', $use_entity_translation);

    list($bid_2, $nodes_2) = $this->createBookOutline('en', $use_entity_translation);
    $fr_nodes_2 = $this->translateNodes($nodes_2, 'fr', $use_entity_translation);

    if (!empty($nodes)) {
      $this->outlineTranslationBlockVisibleOnAllPages($nodes, $nodes_2, $fr_nodes, $fr_nodes_2, $group);
      $this->outlineTranslationBlockVisibleOnBookPages($nodes, $nodes_2, $fr_nodes, $fr_nodes_2, $group);
    }
  }

  /**
   * Test outline translation with the book block visible on ALL pages.
   */
  protected function outlineTranslationBlockVisibleOnAllPages($nodes, $nodes_2, $fr_nodes, $fr_nodes_2, $group = 'Other') {
    variable_set('book_block_mode', 'all pages');

    // Check menu links. Should be found 2 or 3 times each time:
    // - once in the block
    // - once in the breadcrumb
    // - one or zero times in the footer navigation

    // Check English menu links.
    $this->drupalGet('node/' . $nodes[1]->nid);

    $this->assertText($nodes[0]->title, "Found english book 1 root node title ({$nodes[0]->title}), from book navigation, breadcrumb and footer navigation.", 3, $group);
    $this->assertText($nodes_2[0]->title, "Found english book 2 root node title ({$nodes_2[0]->title}), from book navigation.", 1, $group);

    $this->drupalGet('node/' . $nodes[3]->nid);

    $this->assertText($nodes[0]->title, "Found english book 1 root node title ({$nodes[0]->title}), from book navigation and breadcrumb.", 2, $group);
    $this->assertText($nodes_2[0]->title, "Found english book 2 root node title ({$nodes_2[0]->title}), from book navigation.", 1, $group);
    $this->assertText($nodes[1]->title, "Found english book 1 node 1 title ({$nodes[1]->title}), from book navigation and breadcrumb.", 2, $group);
    $this->assertText($nodes[2]->title, "Found english book 1 node 2 title ({$nodes[2]->title}), from book navigation and footer navigation.", 2, $group);
    $this->assertText($nodes[4]->title, "Found english book 1 node 4 title ({$nodes[4]->title}), from book navigation and footer navigation.", 2, $group);
    $this->assertText($nodes[5]->title, "Found english book 1 node 5 title ({$nodes[5]->title}), from book navigation.", 1, $group);

    // Check French menu links.
    $this->drupalGet('fr/node/' . $fr_nodes[1]->nid);

    $this->assertText($fr_nodes[0]->title, "Found french book 1 root node title ({$fr_nodes[0]->title}), from book navigation, breadcrumb and footer navigation.", 3, $group);
    $this->assertText($fr_nodes_2[0]->title, "Found french book 2 root node title ({$fr_nodes_2[0]->title}), from book navigation.", 1, $group);

    $this->drupalGet('fr/node/' . $fr_nodes[3]->nid);

    $this->assertText($fr_nodes[0]->title, "Found french book 1 root node title ({$fr_nodes[0]->title}), from book navigation and breadcrumb.", 2, $group);
    $this->assertText($fr_nodes_2[0]->title, "Found french book 2 root node title ({$fr_nodes_2[0]->title}), from book navigation.", 1, $group);
    $this->assertText($fr_nodes[1]->title, "Found french book 1 node 1 title ({$fr_nodes[1]->title}), from book navigation and breadcrumb.", 2, $group);
    $this->assertText($fr_nodes[2]->title, "Found french book 1 node 2 title ({$fr_nodes[2]->title}), from book navigation and footer navigation.", 2, $group);
    $this->assertText($fr_nodes[4]->title, "Found french book 1 node 4 title ({$fr_nodes[4]->title}), from book navigation and footer navigation.", 2, $group);
    $this->assertText($fr_nodes[5]->title, "Found french book 1 node 5 title ({$fr_nodes[5]->title}), from book navigation.", 1, $group);
  }

  /**
   * Test outline translation with the book block visible on only BOOK pages.
   */
  protected function outlineTranslationBlockVisibleOnBookPages($nodes, $nodes_2, $fr_nodes, $fr_nodes_2, $group = 'Other') {
    variable_set('book_block_mode', 'book pages');

    // Check menu links. Should be found 2 or 3 times each time:
    // - once in the block
    // - once in the breadcrumb
    // - one or zero times in the footer navigation

    // Check English menu links.
    $this->drupalGet('node/' . $nodes[1]->nid);

    $this->assertText($nodes[0]->title, "Found english book 1 root node title, from book navigation, breadcrumb and footer navigation.", 3, $group);
    $this->assertNoText($nodes_2[0]->title, "Found no english book 2 root node title, from book navigation.", $group);

    $this->drupalGet('node/' . $nodes[3]->nid);

    $this->assertText($nodes[0]->title, "Found english book 1 root node title ({$nodes[0]->title}), from book navigation and breadcrumb.", 2, $group);
    $this->assertNoText($nodes_2[0]->title, "Found no english book 2 root node title ({$nodes_2[0]->title}), from book navigation.", $group);
    $this->assertText($nodes[1]->title, "Found english book 1 node 1 title ({$nodes[1]->title}), from book navigation and breadcrumb.", 2, $group);
    $this->assertText($nodes[2]->title, "Found english book 1 node 2 title ({$nodes[2]->title}), from book navigation and footer navigation.", 2, $group);
    $this->assertText($nodes[4]->title, "Found english book 1 node 4 title ({$nodes[4]->title}), from book navigation and footer navigation.", 2, $group);
    $this->assertText($nodes[5]->title, "Found english book 1 node 5 title ({$nodes[5]->title}), from book navigation.", 1, $group);

    // Check French menu links.
    $this->drupalGet('fr/node/' . $fr_nodes[1]->nid);

    $this->assertText($fr_nodes[0]->title, "Found french book 1 root node title ({$fr_nodes[0]->title}), from book navigation, breadcrumb and footer navigation.", 3, $group);
    $this->assertNoText($fr_nodes_2[0]->title, "Found no french book 2 root node title ({$fr_nodes_2[0]->title}), from book navigation.", $group);

    $this->drupalGet('fr/node/' . $fr_nodes[3]->nid);

    $this->assertText($fr_nodes[0]->title, "Found french book 1 root node title ({$fr_nodes[0]->title}), from book navigation and breadcrumb.", 2, $group);
    $this->assertNoText($fr_nodes_2[0]->title, "Found no french book 2 root node title ({$fr_nodes_2[0]->title}), from book navigation.", $group);
    $this->assertText($fr_nodes[1]->title, "Found french book 1 node 1 title ({$fr_nodes[1]->title}), from book navigation and breadcrumb.", 2, $group);
    $this->assertText($fr_nodes[2]->title, "Found french book 1 node 2 title ({$fr_nodes[2]->title}), from book navigation and footer navigation.", 2, $group);
    $this->assertText($fr_nodes[4]->title, "Found french book 1 node 4 title ({$fr_nodes[4]->title}), from book navigation and footer navigation.", 2, $group);
    $this->assertText($fr_nodes[5]->title, "Found french book 1 node 5 title ({$fr_nodes[5]->title}), from book navigation.", 1, $group);
  }

  /**
   * Display a message to the user running the current tests.
   *
   * @see i18n_book_navigation_test_group_finished()
   *
   * @param string $message
   *   The message to display to the user.
   * @param string $type
   *   (optional) The type of the message. Can be 'status', 'warning' or
   *   'error'. Defaults to 'status'.
   */
  protected function setMessage($message, $type = 'status') {
    // Use the live database.
    db_set_active('simpletest_original_default');

    $messages = variable_get('i18n_book_navigation_test_messages', array());

    $messages[] = array(
      'message' => $message,
      'type' => $type,
    );

    variable_set('i18n_book_navigation_test_messages', $messages);

    // Switch back to the database currently used by Simpletest.
    db_set_active('default');
  }
}

class I18nBookNavigationTestCase extends I18nBookNavigationTestBase {

  /**
   * {@inheritDoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Book translation',
      'description' => 'Test that the book translation works properly and translates the navigation as expected.',
      'group' => 'Internationalization',
    );
  }

  /**
   * Test core logic.
   */
  public function testCoreLogic() {
    // Create 2 books.
    list(, $nodes) = $this->createBookOutline();
    $this->translateNodes($nodes, 'fr');

    list(, $nodes_2) = $this->createBookOutline();
    $this->translateNodes($nodes_2, 'fr');

    $num_books = count(book_get_books());

    $this->drupalGet('/node');

    $this->assertEqual(count(i18n_book_navigation_get_books()), $num_books, "Find the same number of books when in english ($num_books books).", 'testCoreLogic');

    $this->drupalGet('/fr/node');

    $this->assertEqual(count(i18n_book_navigation_get_books()), $num_books, "Find the same number of books when in french ($num_books books).", 'testCoreLogic');
  }

  /**
   * Test outline translations.
   */
  public function testOutlineTranslation() {
    $this->outlineTranslation(FALSE, 'testOutlineTranslation');
  }

  /**
   * Test i18n_menu integration.
   */
  public function testI18nMenuIntegration() {
    module_enable(array('i18n_menu'));

    $this->resetAll();

    // Test outline translation.
    $this->outlineTranslation(FALSE, 'testI18nMenuIntegration');
  }

  /**
   * Test i18n_select integration.
   */
  public function testI18nSelectIntegration() {
    module_enable(array('i18n_select'));

    $this->resetAll();

    // Test outline translation.
    $this->outlineTranslation(FALSE, 'testI18nSelectIntegration');
  }

  /**
   * Regression test for [#1619026].
   *
   * @see http://drupal.org/node/1619026#comment-6276880
   */
  public function testIssue1619026() {
    // Step 1 and 2 -> self::setUp().

    // Step 3.
    list($bid, $nodes) = $this->createBookOutline();

    // Step 4.
    $fr_nodes = $this->translateNodes($nodes, 'fr');

    // Step 5.
    module_enable(array('i18n_menu', 'i18n_select'));

    $this->resetAll();

    // Step 6.
    $this->enableLanguages(array('de'));

    // Step 7.
    $de_nodes = $this->translateNodes($nodes, 'de');

    // Step 8.
    // Check English menu links.
    $this->drupalGet('node/' . $nodes[1]->nid);

    $this->assertText($nodes[0]->title, "Found english root node title, from book navigation, breadcrumb and footer navigation.", 3, 'testIssue1619026');
    $this->assertText($nodes[1]->title, "Found english node 1 title, from book navigation and page title.", 2, 'testIssue1619026');

    // Check French menu links.
    $this->drupalGet('fr/node/' . $fr_nodes[1]->nid);

    $this->assertText($fr_nodes[0]->title, "Found french root node title, from book navigation, breadcrumb and footer navigation.", 3, 'testIssue1619026');

    $this->drupalGet('fr/node/' . $fr_nodes[3]->nid);

    $this->assertText($fr_nodes[0]->title, "Found french root node title, from book navigation and breadcrumb.", 2, 'testIssue1619026');

    // Check German menu links.
    $this->drupalGet('de/node/' . $de_nodes[1]->nid);

    $this->assertText($de_nodes[0]->title, "Found german root node title, from book navigation, breadcrumb and footer navigation.", 3, 'testIssue1619026');

    $this->drupalGet('de/node/' . $de_nodes[3]->nid);

    $this->assertText($de_nodes[0]->title, "Found german root node title, from book navigation and breadcrumb.", 2, 'testIssue1619026');
  }
}


class I18nBookNavigationMenuBlockTestCase extends I18nBookNavigationTestBase {

  /**
   * {@inheritDoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Book translation Menu Block integration',
      'description' => 'Test that the book translation integrates properly with the Menu Block module.',
      'group' => 'Internationalization',
    );
  }

  /**
   * Test menu_block integration.
   */
  public function testMenuBlockIntegration() {
    if (module_enable(array('menu_block'))) {
      $this->resetAll();

      // Create user again - needs new permission for menu_block.
      $this->admin_user = $this->drupalCreateUser(array_keys(module_invoke_all('permission')));

      $this->drupalLogin($this->admin_user);

      $this->disableI18nBookNavigationBlock();

      list($bid, $nodes) = $this->createBookOutline();
      $fr_nodes = $this->translateNodes($nodes, 'fr');

      list($bid_2, $nodes_2) = $this->createBookOutline();
      $fr_nodes_2 = $this->translateNodes($nodes_2, 'fr');

      // Add menu block.
      $this->configureAndEnableMenuBlock($nodes[0]->book['menu_name']);

      // Test outline translation.
      if (!empty($nodes)) {
        $this->outlineTranslationBlockVisibleOnBookPages($nodes, $nodes_2, $fr_nodes, $fr_nodes_2, 'testMenuBlockIntegration');
      }
    }
    else {
      $this->fail("The Menu Block tests were not run, as the module could not be enabled. Make sure the Menu Block module is available.");
    }
  }

  /**
   * Set up the i18n_book_navigation menu_block.
   */
  protected function configureAndEnableMenuBlock($menu_name) {
    $this->drupalGet('admin/structure/block/add-menu-block');

    $edit = array(
      'title' => 'Menu Block',
      'admin_title' => 'Menu Block',
      'menu_name' => $menu_name,
      'parent' => "$menu_name:0",
      'regions[bartik]' => 'sidebar_first',
    );

    $this->drupalPost('admin/structure/block/add-menu-block', $edit, t("Save block"));
  }
}

class I18nBookNavigationPanelsTestCase extends I18nBookNavigationTestBase {

  /**
   * {@inheritDoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Book translation Panels integration',
      'description' => 'Test that the book translation integrates properly with the Panels module.',
      'group' => 'Internationalization',
    );
  }

  /**
   * Test panels integration.
   */
  public function testPanelsIntegration() {
    if (module_enable(array('page_manager', 'panels'))) {
      $this->resetAll();

      // Refresh and login user.
      $this->admin_user = $this->drupalCreateUser(array_keys(module_invoke_all('permission')));
      $this->drupalLogin($this->admin_user);

      list($bid, $nodes) = $this->createBookOutline();
      $fr_nodes = $this->translateNodes($nodes, 'fr');

      list($bid_2, $nodes_2) = $this->createBookOutline();
      $fr_nodes_2 = $this->translateNodes($nodes_2, 'fr');

      // Activate panel.
      $this->drupalGet('admin/structure/pages');
      // Find the "node_view" enable link. We have to regex this one out, as it
      // can move on the admin page (not always at the same place).
      $content = $this->drupalGetContent();
      $match;
      preg_match('/href=".*(admin\/structure\/pages\/nojs\/enable\/node_view\?.+)"/', $content, $match);
      list($url, $query) = explode('?', $match[1]);
      $this->drupalGet('admin/structure/pages/nojs/enable/node_view', array(
        'query' => array('token' => str_replace('token=', '', $query)),
      ));
      $this->drupalGet('admin/structure/pages/edit/node_view');
      $this->clickLink(t("Import variant"));

      $edit = array(
        'object' => file_get_contents(drupal_get_path('module', 'i18n_book_navigation') . '/tests/fixture/ctools.default_panel.txt'),
      );

      $this->drupalPost('admin/structure/pages/nojs/operation/node_view/actions/import', $edit, t("Update and save"));

      // Test outline translation.
      if (!empty($nodes)) {
        $this->outlineTranslationBlockVisibleOnAllPages($nodes, $nodes_2, $fr_nodes, $fr_nodes_2, 'testPanelsIntegration');
        $this->outlineTranslationBlockVisibleOnBookPages($nodes, $nodes_2, $fr_nodes, $fr_nodes_2, 'testPanelsIntegration');
      }
    }
    else {
      $this->fail("The Panels tests were not run, as the module could not be enabled. Make sure the Panels module is available.");
    }
  }
}

class I18nBookNavigationEntityTranslationTestCase extends I18nBookNavigationTestBase {

  /**
   * {@inheritDoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Book translation Entity Translation integration',
      'description' => 'Test that the book translation integrates properly with the Entity Translation module.',
      'group' => 'Internationalization',
    );
  }

  /**
   * Test entity_translation and title integration.
   */
  public function testEntityTranslationIntegration() {
    module_disable(array('translation'), FALSE);

    if (module_enable(array('entity_translation', 'title'))) {
      $this->resetAll();

      $this->configureI18nDetection(TRUE);
      $this->makeBooksTranslatable(TRUE);

      // Test outline translation.
      $this->outlineTranslation(TRUE, 'testEntityTranslationIntegration');
    }
    else {
      $this->fail("The Entity translation tests were not run, as the module could not be enabled. Make sure the Entity Translation and Title modules are available.");
    }
  }
}